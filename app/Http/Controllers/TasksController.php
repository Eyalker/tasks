<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        /**if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        **/
        if (Auth::check()) {
            // The user is logged in...
        
        $id= Auth::id();
        $user = User::find($id);
        $tasks = $user->tasks;
        return view('tasks.index', compact('tasks'));
        }
        return redirect()->intended('/home');
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $id = Auth::id(); //the id of the current user
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status = 0;
        $task->save();
        return redirect('tasks');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //only if this todo belongs to user
       $task = Task::find($id);
       //employees are not allowed to change the title 
    /*    if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }   
    */
        //make sure the todo belongs to the logged in user
   //     if(!$task->user->id == Auth::id()) return(redirect('tasks'));
   
        //test if title is dirty
       
        $task->update($request->except(['_token']));
   
       if($request->ajax()){
         if (Gate::denies('admin')) {
             abort(403,"You are not allowed to check tasks");
        }
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);
        } else {          
            return redirect('tasks');           
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to delete tasks");
       }
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');
    }

    public function mytasks()
    { 
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = $user->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
    }
}
